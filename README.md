
# General Guidelines for ROCON contributions 

## Introduction

This guide is a read once, make everyone's life easier type of document. 
Hopefully, it's implementation will make everybody's life easier and will solve 
a lot of management and coworking problems. 

In essence, this guide will explain how to write clean-ish code, how to name 
projects such that they are easy to find, where to store and how to comment. 

New guidelines will be continuously added as new organizational features will be 
implemented. 

## 1. General coding conventions:

All code should be written as clean as possible and should also include frequent 
comments that document how the code works.

**If possible, the repos should implement DOXYGEN**

A width of maximum 80 columns should be used for better formatting.

**IT is especially important to have a consistent naming convention for your code.**

The coding styles that are going to be imposed are going to be industry
standards. Each programming language has it's own standard:

- [C](https://users.ece.cmu.edu/~eno/coding/CCodingStandard.html)
- [C++](https://google.github.io/styleguide/cppguide.html)
- [python](https://google.github.io/styleguide/pyguide.html)
- [matlab](https://www.ee.columbia.edu/~marios/matlab/MatlabStyle1p5.pdf) 

## 2. Repository naming conventions:

All code should be also be frequently pushed on a github/gitlab repository and 
once in a while on the department's file server in order to make admin. happy. 

All repository names should be lowercase and dashed in naming.

The general standard for the naming of a repo is:

_[technology]-[department]-[functionality/project]_ 

This means for example:

- ros-rocon-turtlebot
- matlab-rocon-turtlebot

For a project that requires multiple technologies, a bigger repo can be made 
that links through submodules the other repos. 

Each repo should have a main branch and a development brach. The branches sould 
be merged only if the updates on the development branch are tested and stable.


Each repo should be considered as a module for later projects. I.E. a repository
that contains code for the ros turtlebot could lated be implemented in later
projects and also could be modified or forked.


## 3. Tooling and README writing:

Each project should include in it's README.md file a quick explanation on how to 
setup the development environment and which tools are needed. In general, a good
rule of thumb is having VS Code and MATLAB installed on every development 
machine. 

It is also a good practice, if possible, to create installation scripts for the
environment such that it is easier to work with.

A good practice is also using linux as a direct machine or as a virtual machine.
